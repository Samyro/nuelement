Pod::Spec.new do |s|

  s.name         = "NUElement"
  s.version      = "0.1.0"
  s.summary      = "Customized element for Nestia User App."
  s.homepage     = "https://bitbucket.org/nestiasg/nuelement"
  s.author             = { "Samyro" => "rewritealphabet@gmail.com" }
  s.source       = { :git => "git@bitbucket.org:nestiasg/nuelement.git", :tag => '0.1.0' }

  s.source_files  = "Sources", "NUElement/**/*.{swift}"
  s.exclude_files = "NUElement/**/ARLogger.swift"
  s.resources = "Resources", "NUElement/**/*.{xib,xcassets,storyboard,strings}"

end
