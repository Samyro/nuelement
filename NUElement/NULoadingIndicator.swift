//
//  NULoadingIndicator.swift
//  Template
//
//  Created by ZhangHanlin on 8/7/16.
//  Copyright © 2016 ZhangHanlin. All rights reserved.
//

import UIKit

open class NULoadingIndicator: UIView {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var loadingIndicatorContainer: UIView!
    @IBOutlet weak var reloadContainer: UIView!
    
    @IBOutlet open weak var loadingIndicatorLabel: UILabel!
    @IBOutlet open weak var failToReloadLabel: UILabel!
    @IBOutlet open weak var pressToReloadLabel: UILabel!
    @IBOutlet open weak var backGroundContainer: UIView!
    @IBOutlet weak var shadowBgView: UIView!
    
    weak var timer: Timer!
    
    @IBOutlet open weak var reloadBtn: UIButton!
    
    fileprivate var shouldEnd = false
    fileprivate var shouldDestory = false
    fileprivate var closeNow = false
    fileprivate var maxShowTime: Float = 1.5
    fileprivate var startShowTime: Float = 0.5
    
    open class func initFromNib() -> NULoadingIndicator {
        
        return UINib(nibName: "NULoadingIndicator", bundle: Bundle(for: NULoadingIndicator.self)).instantiate(withOwner: nil, options: nil).first as! NULoadingIndicator
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        let imgNamePrefix = "NULoading"
        var imgArr = [UIImage]()
        
        for i in 0...5 {
            imgArr.append(UIImage(named: imgNamePrefix + "\(i)", in: Bundle(for: NULoadingIndicator.self), compatibleWith: nil)!)
        }
        
        imgView.animationImages = imgArr
        imgView.animationDuration = 0.4
        imgView.animationRepeatCount = 0
        
        
        loadingIndicatorLabel.tag = 0
        
        reloadContainer.isHidden = true
        loadingIndicatorContainer.isHidden = true
        
        backGroundContainer.layer.cornerRadius = 8
        backGroundContainer.clipsToBounds = true
        
        
        shadowBgView.layer.shadowColor = UIColor(red: 169/255, green: 181/255, blue: 194/255, alpha: 1).cgColor
        shadowBgView.layer.cornerRadius = 8
        shadowBgView.layer.shadowRadius = 8
        shadowBgView.layer.shadowOpacity = 1
        
        
        loadingIndicatorLabel.text = NSLocalizedString("nu_loading", tableName: nil, bundle: Bundle(for: NULoadingIndicator.self), value: "", comment: "")
        failToReloadLabel.text = NSLocalizedString("nu_fail_to_load", tableName: nil, bundle: Bundle(for: NULoadingIndicator.self), value: "", comment: "")
        pressToReloadLabel.text = NSLocalizedString("nu_press_to_reload", tableName: nil, bundle: Bundle(for: NULoadingIndicator.self), value: "", comment: "")
        
        isHidden = true
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        if let theSuperView = newSuperview {
            self.frame.size = CGSize(width: 100, height: 80)
            self.center = theSuperView.center
        }
    }
    
    override open func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        stopLoadingAnimation()
        //        ARLogger.debug("did move to super view")
        
    }
    
    open func setShowTime(startTime start: Float, maxInterval: Float) {
        startShowTime = start
        if startShowTime < 0.1 {
            startShowTime = 0.1
        }
        
        if maxInterval < 0.7 {
            maxShowTime = 0.7
        } else {
            maxShowTime = maxInterval
        }
    }
    
    open func startLoadingAnimation(_ withBackground: Bool = false) {
        reloadContainer.isHidden = true
        loadingIndicatorContainer.isHidden = false
        imgView.stopAnimating()
        if timer != nil {
            timer.invalidate()
        }
        
        imgView.startAnimating()
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(animateNextLabel), userInfo: nil, repeats: true)
        timer.fire()
        if withBackground {
            showBackground()
        }
        
        shouldEnd = false
        shouldDestory = false
        closeNow = false
        
        dispatch(startShowTime) { [weak self] in
            self?.firstCheck()
        }
    }
    
    open func stopLoadingAnimation() {
        loadingIndicatorContainer.isHidden = true
        hideBackground()
        imgView.stopAnimating()
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        
        if closeNow {
            isHidden = true
            
            if shouldDestory {
                clean()
            }
        } else {
            shouldEnd = true
        }
    }
    
    open func showReloadBtn() {
        loadingIndicatorContainer.isHidden = true
        self.isUserInteractionEnabled = true
        reloadContainer.isHidden = false
    }
    
    open func hideReloadBtn() {
        self.isUserInteractionEnabled = false
        reloadContainer.isHidden = true
    }
    
    open func showBackground() {
        shadowBgView.isHidden = false
        backGroundContainer.isHidden = false
    }
    
    open func hideBackground() {
        shadowBgView.isHidden = true
        backGroundContainer.isHidden = true
    }
    
    func animateNextLabel() {
        var text = NSLocalizedString("nu_loading", tableName: nil, bundle: Bundle(for: NULoadingIndicator.self), value: "", comment: "") + " "
        for _ in 0..<loadingIndicatorLabel.tag {
            text = text + "."
        }
        loadingIndicatorLabel.tag = (loadingIndicatorLabel.tag+1)%4
        loadingIndicatorLabel.text = text
    }
    
    deinit {
        if timer != nil {
            timer.invalidate()
        }
        //        ARLogger.debug("deinit nu loading indicator")
    }
    
    open override func removeFromSuperview() {
        shouldEnd = true
        shouldDestory = true
        stopLoadingAnimation()
    }
    
    func firstCheck() {
        if shouldEnd {
            isHidden = true
            
            if shouldDestory {
                clean()
            }
        } else {
            isHidden = false
            dispatch(maxShowTime-0.5) { [weak self] in
                self?.secondCheck()
            }
        }
    }
    
    func secondCheck() {
        if shouldEnd {
            isHidden = true
            
            if shouldDestory {
                clean()
            }
        } else {
            closeNow = true
        }
    }
    
    func clean() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        super.removeFromSuperview()
    }
    
    func dispatch(_ time: Float, closure: @escaping ()->Void ) {
        let t = time * Float(NSEC_PER_SEC)
        let _time = DispatchTime.now() + Double(Int64(UInt64(t))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: _time, execute: closure)
    }
    
}
