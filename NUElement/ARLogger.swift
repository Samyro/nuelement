//
//  ARLogger.swift
//  Arriving-iOS
//
//  Created by ZhangHanlin on 31/5/15.
//  Copyright (c) 2015 TRIO. All rights reserved.
//

import UIKit

class ARLogger {
  
  static let FATAL_LEVEL = 1000
  static let ERROR_LEVEL = 900
  static let WARN_LEVEL = 800
  static let INFO_LEVEL = 700
  static let DEBUG_LEVEL = 600
  static let TRACE_LEVEL = 500
  
  static var LOG_LEVEL = TRACE_LEVEL
  
  static func fatal(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      
      if LOG_LEVEL <= FATAL_LEVEL {
        log("FATAL",message: message,file: file,line: line,function: function, verbose: true)
      }
      
  }
  
  static func error(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      if LOG_LEVEL <= ERROR_LEVEL {
        log("ERROR",message: message,file: file,line: line,function: function, verbose: true)
      }
  }
  
  static func warn(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      if LOG_LEVEL <= WARN_LEVEL {
        log("WARN",message: message,file: file,line: line,function: function, verbose: true)
      }
  }
  
  static func info(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      if LOG_LEVEL <= INFO_LEVEL {
        log("INFO",message: message,file: file,line: line,function: function, verbose: true)
      }
  }
  
  static func debug(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      if LOG_LEVEL <= DEBUG_LEVEL {
        log("DEBUG",message: message,file: file,line: line,function: function, verbose: true)
      }
  }
  
  static func trace(
    _ message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function) {
      if LOG_LEVEL <= TRACE_LEVEL {
        log("TRACE",message: message,file: file,line: line,function: function, verbose: true)
      }
  }
  
  static func log (
    _ prefix: String,
    message: Any = "",
    file: String = #file,
    line: UInt = #line,
    function: String = #function,
    verbose: Bool) {
      
      _ = Date()
      let aDateFormat = DateFormatter()
      aDateFormat.dateFormat = "YYYY-MM-dd hh:mm:ss"
      let time = aDateFormat.string(from: Date())
      
      let fileName = file.components(separatedBy: "/").last as String!
      
      if verbose {
        print("[\(prefix)] \(time) IN \(fileName) LINE: \(line) FUNC: \(function) - \(message)")
      }else{
        print("[\(prefix)] \(time) \(message)")
      }
      
  }
  
}
